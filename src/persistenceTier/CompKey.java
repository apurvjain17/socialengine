package persistenceTier;

import java.io.Serializable;

public class CompKey implements Serializable {

	String userName;
	int pid;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public boolean equals(Object obj){
		
		if(obj instanceof CompKey){
			CompKey temp=(CompKey)obj;
			if(temp.pid==pid){
				return temp.userName.equals(userName);
			}
		}
		return false;
	}
	public int hashCode(){
		return userName.hashCode()+pid;
	}
}
