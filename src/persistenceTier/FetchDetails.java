package persistenceTier;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import presentationTier.CommentTO;
import presentationTier.LikeTO;
import presentationTier.PostTO;
import presentationTier.UserTO;

public class FetchDetails {

	public UserTO getDetails(String userName){
		
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		User u=em.find(User.class, userName);
		UserTO uto=null;
		if(u!=null){
			uto=new UserTO();
			uto.setBirthDate(u.getBirthDate());
			uto.setEmail(u.getEmail());
			uto.setFirstName(u.getFirstName());
			uto.setLastName(u.getLastName());
			uto.setGender(u.getGender());
			uto.setPassword(u.getPassword());
			uto.setUserName(u.getUserName());
		}
		em.close();
		emf.close();
		return uto;
	}

	public boolean saveDetails(UserTO uto) {
		// TODO Auto-generated method stub
		User usr=new User();
		usr.setBirthDate(uto.getBirthDate());
		usr.setEmail(uto.getEmail());
		usr.setFirstName(uto.getFirstName());
		usr.setGender(uto.getGender());
		usr.setLastName(uto.getLastName());
		usr.setUserName(uto.getUserName());
		usr.setPassword(uto.getPassword());
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		User temp=em.find(User.class, uto.getUserName());
		boolean flag=false;
		if(temp==null){
			em.getTransaction().begin();
			em.persist(usr);
			em.getTransaction().commit();
			flag=true;
		}
		em.close();
		emf.close();
		return flag;
	}

	public void insert(PostTO post) {
		// TODO Auto-generated method stub
		PostEntity pe=new PostEntity();
		/*System.out.println(post.getTimestamp());*/
		pe.setCommentcnt(post.getCommentcnt());
		pe.setLikecnt(post.getLikecnt());
		pe.setPost(post.getPost());
		pe.setTimestamp(post.getTimestamp());
		pe.setUserName(post.getUserName());
		/*System.out.println(pe.getTimestamp());*/
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(pe);
		em.getTransaction().commit();
		em.close();
		emf.close();
		
	}

	public List<PostTO> getPosts(String userName) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		Query q=em.createQuery("select pt from PostEntity pt where pt.userName=:arg1");
		q.setParameter("arg1", userName);
		/*System.out.println(userName);*/
		List<PostEntity>list=q.getResultList();
		/*System.out.println(list.size());*/
		List<PostTO> list2=new ArrayList<PostTO>();
		if(list!=null){
			for(PostEntity pe:list){
				PostTO pto=new PostTO();
				pto.setCommentcnt(pe.getCommentcnt());
				pto.setLikecnt(pe.getLikecnt());
				pto.setPost(pe.getPost());
				pto.setPostId(pe.getPostid());
				pto.setTimestamp(pe.getTimestamp());
				pto.setUserName(pe.getUserName());
				list2.add(pto);
			}
		}
		em.close();
		emf.close();
		return list2;
	}

	public List<PostTO> searchUser(UserTO usr) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		User usrtemp=em.find(User.class, usr.getUserName());
		if(usrtemp!=null){
			usr.setBirthDate(usrtemp.getBirthDate());
			usr.setEmail(usrtemp.getEmail());
			usr.setFirstName(usrtemp.getFirstName());
			usr.setLastName(usrtemp.getLastName());
			usr.setGender(usrtemp.getGender());
			List<PostTO>list= getPosts(usr.getUserName());
			/*System.out.println(list.size());*/
			return list;
		}
		em.close();
		emf.close();
		return null;
	}

	public int likePost(LikeTO liketo) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		CompKey ck=new CompKey();
		
		ck.setPid(liketo.getPid());
		ck.setUserName(liketo.getUserName());
		LikeEntity le=em.find(LikeEntity.class, ck);
		PostEntity pe=em.find(PostEntity.class, liketo.getPid());
		if(le==null){
			le=new LikeEntity();
			le.setPid(liketo.getPid());
			le.setUserName(liketo.getUserName());
			em.getTransaction().begin();
			em.persist(le);
			em.getTransaction().commit();
			em.getTransaction().begin();
			pe.setLikecnt(pe.getLikecnt()+1);
			em.getTransaction().commit();
			em.close();
			emf.close();
		}
		return pe.getLikecnt();
	}

	public int disLikePost(LikeTO liketo) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		CompKey ck=new CompKey();
		
		ck.setPid(liketo.getPid());
		ck.setUserName(liketo.getUserName());
		LikeEntity le=em.find(LikeEntity.class, ck);
		PostEntity pe=em.find(PostEntity.class, liketo.getPid());
		if(le!=null){
			em.remove(le);
			em.getTransaction().begin();
			pe.setLikecnt(pe.getLikecnt()-1);
			em.getTransaction().commit();
			em.close();
			emf.close();
		}
		return pe.getLikecnt();
	}

	public void persistComment(CommentTO com) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		Comments ce=new Comments();
		ce.setComment(com.getComment());
		ce.setPostid(com.getPostid());
		ce.setUserName(com.getUserName());
		PostEntity pe=em.find(PostEntity.class, com.getPostid());
		em.getTransaction().begin();
		em.persist(ce);
		pe.setCommentcnt(pe.getCommentcnt()+1);
		em.getTransaction().commit();
		em.close();
		emf.close();
	}

	public PostTO fetchPost(int postid) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		PostEntity pe=em.find(PostEntity.class, postid);
		PostTO pto=new PostTO();
		pto.setCommentcnt(pe.commentcnt);
		pto.setLikecnt(pe.getLikecnt());
		pto.setPost(pe.getPost());
		pto.setPostId(pe.getPostid());
		pto.setTimestamp(pe.getTimestamp());
		pto.setUserName(pe.getUserName());
		return pto;
	}

	public List<CommentTO> fetchCommentList(int postid) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf=Persistence.createEntityManagerFactory("Kuliza2");
		EntityManager em=emf.createEntityManager();
		Query q=em.createQuery("select cmnt from Comments cmnt where cmnt.postid=:arg1");
		q.setParameter("arg1", postid);
		/*System.out.println(userName);*/
		List<Comments>list=q.getResultList();
		/*System.out.println(list.size());*/
		List<CommentTO> list2=new ArrayList<CommentTO>();
		if(list!=null){
			for(Comments ce:list){
				CommentTO cto=new CommentTO();
				cto.setComment(ce.getComment());
				cto.setCommentid(ce.getCommentid());
				cto.setPostid(ce.getPostid());
				cto.setUserName(ce.getUserName());
				list2.add(cto);
			}
		}
		em.close();
		emf.close();
		return list2;
	}
}
