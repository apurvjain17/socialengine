package test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Tester {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd",Locale.US);
		System.out.println(sdf.parse("1992-08-17"));
	}
}
