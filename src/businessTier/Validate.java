package businessTier;

import java.util.List;

import persistenceTier.FetchDetails;
import presentationTier.CommentTO;
import presentationTier.LikeTO;
import presentationTier.PostTO;
import presentationTier.UserTO;

public class Validate {

	public boolean chck(UserTO cto){
		FetchDetails ftd=new FetchDetails();
		UserTO uto=ftd.getDetails(cto.getUserName());
		if(uto==null){
			return false;
		}
		else{
			return uto.getPassword().equals(cto.getPassword());
		}
	}
	public boolean signUp(UserTO uto){
		FetchDetails ftd=new FetchDetails();
		return ftd.saveDetails(uto);
	}
	public void insert(PostTO post) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		fd.insert(post);
	}
	public List<PostTO> getPosts(String userName) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		List<PostTO> list=fd.getPosts(userName);		
		return list;
	}
	public List<PostTO> searchUser(UserTO usr) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		List<PostTO> list=fd.searchUser(usr);
		/*System.out.println(list.size());*/
		return list;
	}
	public int likePost(LikeTO liketo) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		int x=fd.likePost(liketo);
		return x;
	}
	public int disLikePost(LikeTO liketo) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		int x=fd.disLikePost(liketo);
		return x;
	}
	public void persistComment(CommentTO com) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		fd.persistComment(com);
	}
	public PostTO fetchPost(int postid) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		return fd.fetchPost(postid);
	}
	public List<CommentTO> fetchCommentList(int postid) {
		// TODO Auto-generated method stub
		FetchDetails fd=new FetchDetails();
		return fd.fetchCommentList(postid);
	}
}
