package presentationTier;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import businessTier.Validate;

public class Search extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
		HttpSession session=req.getSession(false);
		String userName=null;
		if(session==null){
			res.sendRedirect("login.jsp");
		}
		else{
			userName=(String)session.getAttribute("userName");
		}
		String str=(String)req.getParameter("search");
		Validate val=new Validate();
		UserTO usr=new UserTO();
		usr.setUserName(str);
		List<PostTO>list=val.searchUser(usr);
		req.setAttribute("user", usr);
		req.setAttribute("list", list);
		/*System.out.println(list.size());*/
		req.getRequestDispatcher("search.jsp").forward(req, res);
	}
}
