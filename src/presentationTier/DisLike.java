package presentationTier;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import businessTier.Validate;

public class DisLike extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException{
		HttpSession session=req.getSession(false);
		String userName=null;
		if(session==null){
			res.sendRedirect("login.jsp");
		}
		else{
			userName=(String)session.getAttribute("userName");
		}
		int pid=Integer.parseInt(req.getParameter("postid"));
		userName=req.getParameter("userName");
		PrintWriter pw=res.getWriter();
		Validate val=new Validate();
		LikeTO liketo=new LikeTO();
		liketo.setPid(pid);
		liketo.setUserName(userName);
		int x=val.disLikePost(liketo);
		System.out.println(x);
		pw.write(Integer.toString(x)+" ");
		pw.close();
	}
}
