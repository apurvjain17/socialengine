package presentationTier;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.misc.BASE64Encoder;

import com.mysql.fabric.Response;
import com.sun.xml.internal.org.jvnet.staxex.Base64EncoderStream;

import businessTier.Validate;

public class Authenticate extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		
		UserTO uto=new UserTO();
		uto.setUserName(req.getParameter("userName"));
		/*System.out.println(uto.getUserName()+"1");*/
		uto.setPassword(req.getParameter("password"));
		Validate val=new Validate();
		boolean result=val.chck(uto);
		if(result){
			/*System.out.println(uto.getUserName()+"2");*/
			HttpSession session=req.getSession();
			session.setAttribute("userName", uto.getUserName());
			session.setAttribute("user", uto);
			res.sendRedirect("retrieve");
		}
		else{
			res.sendRedirect("login.jsp?id=2");
		}
	}
	
}
