package presentationTier;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import businessTier.Validate;

public class SignUp extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException{
	
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd",Locale.US);
		String date=(String)req.getParameter("birthDate");
		String password=(String)req.getParameter("password");
		String cpassword=(String)req.getParameter("cpassword");
		String userName=(String)req.getParameter("userName");
		String email=(String)req.getParameter("email");
		String firstName=(String)req.getParameter("firstName");
		String lastName=(String)req.getParameter("lastName");
		boolean flag=true;
		int index=0;
		if(date.trim().equals("") || lastName.trim().equals("") || firstName.trim().equals("") ||email.trim().equals("") || password.trim().equals("") || userName.trim().equals("")){
			index=5;
			flag=false;
		}
		
		if(!cpassword.equals(password)){
			flag=false;
			index=3;
		}
		/*System.out.println("--------------------------------------------"+userName+"---------------------");*/
		UserTO ust=new UserTO();
		ust.setPassword(password);
		ust.setUserName(userName);
		try {
			ust.setBirthDate(sdf.parse(date));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			flag=false;
			index=4;
		}
		
		ust.setEmail(email);
		ust.setFirstName(firstName);
		ust.setLastName(lastName);
		ust.setGender((String)req.getParameter("gender"));
		Validate val=new Validate();
		if(flag)
		flag=val.signUp(ust);
		if(flag){
			res.sendRedirect("login.jsp?id="+index);
		}
		else{
			res.sendRedirect("login.jsp?id="+index);
		}
			
	}
}
