package presentationTier;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import businessTier.Validate;
import sun.misc.BASE64Decoder;


public class Retrieve extends HttpServlet{

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws UnsupportedEncodingException, IOException, ServletException{
		HttpSession session=req.getSession(false);
		String userName=null;
		if(session==null){
			res.sendRedirect("login.jsp");
		}
		else{
			userName=(String)session.getAttribute("userName");
			Validate val=new Validate();
			List<PostTO> list=val.getPosts(userName);
			/*System.out.println(list.size());*/
			List<PostTO> list2=new ArrayList<PostTO>();
			for(int x=list.size()-1;x>=0;x--){
				list2.add(list.get(x));
			}
			req.setAttribute("list",list2);
			req.getRequestDispatcher("home.jsp").forward(req, res);
		}
		
		
		
	}
}
