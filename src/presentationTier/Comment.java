package presentationTier;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import businessTier.Validate;

public class Comment extends HttpServlet {

	public void doPost(HttpServletRequest req , HttpServletResponse res) throws IOException, ServletException{
		HttpSession session=req.getSession(false);
		String userName=null;
		if(session==null){
			res.sendRedirect("login.jsp");
		}
		else{
			userName=(String)session.getAttribute("userName");
		}
		String comment=req.getParameter("comment");
		Validate val=new Validate();
		int postid=Integer.parseInt(req.getParameter("postid"));
		if(comment!=null){
			CommentTO com=new CommentTO();
			com.setComment(comment);
			com.setPostid(postid);
			com.setUserName(userName);
			val.persistComment(com);
		}
		PostTO pto=val.fetchPost(postid);
		List<CommentTO> list=val.fetchCommentList(postid);
		/*System.out.println("comment servlet "+list.size());
		System.out.println("comment servlet printing post "+pto.getPost());*/
		req.setAttribute("list", list);
		req.setAttribute("post", pto);
		req.getRequestDispatcher("comment.jsp").forward(req, res);
	}
}
