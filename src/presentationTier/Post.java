package presentationTier;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sun.misc.BASE64Decoder;
import businessTier.Validate;

import com.mysql.jdbc.util.Base64Decoder;

public class Post extends HttpServlet{

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws UnsupportedEncodingException, IOException, ServletException{
		HttpSession session=req.getSession(false);
		String userName=null;
		if(session==null){
			res.sendRedirect("login.jsp");
		}
		else{
			userName=(String)session.getAttribute("userName");
		}
		PostTO post=new PostTO();
		post.setCommentcnt(0);
		post.setLikecnt(0);
		post.setPost((String)req.getParameter("post"));
		post.setUserName(userName);
		post.setTimestamp(Calendar.getInstance().getTime());
		/*System.out.println(post.getTimestamp());*/
		Validate val=new Validate();
		val.insert(post);
		res.sendRedirect("retrieve");
	}
}
