<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SocialEngine1.0</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<style>
body{
font-family: 'Open Sans', sans-serif;
}
.topPanel{
	top:0%;
	left:0%;
	padding:0%;
	margin:0%;
	background-color: black;
	color:azure;
	height: 10%;
	width: 100%;
	position:fixed;
}
#form1{
	
	padding:0.5%;
	margin:0.5%;
	position:relative;

}
#se{
font-family: 'Indie Flower', cursive;
}
#form2{
	position:fixed;
	top:30%;
	left:5%;
	font-size: 15pt;
}
</style>
<link href='https://fonts.googleapis.com/css?family=Indie+Flower' rel='stylesheet' type='text/css'>
</head>
<body background="bg1.gif">
<%
	String userName=(String)session.getAttribute("userName");
	if(userName!=null){
		response.sendRedirect("retrieve");
	}
%>
<div class="topPanel">
<form id="form1" class="form-inline" action="authenticate" method="post">
<label>UserName</label> <input class="form-control" type="text" name="userName" placeholder="UserName"/>
<t/> <label>Password</label><input class="form-control" type="password" name="password" placeholder="Password"/>
<input type="submit" class="btn btn-success" name="login" value="Login"/>
</form>
</div>

<form id="form2" action='signUp' method='post'>
<table>
<h1 id="se">Social Engine 1.0</h1>
<tr><td>UserName:<font color="red">*</font></td><td><input class="form-control" type="text" name="userName" placeholder="UserName"/></td><tr/>
<tr><td>FirstName:<font color="red">*</font></td><td><input class="form-control" type="text" name="firstName" placeholder="FirstName"/></td><tr/>
<tr><td>LastName:<font color="red">*</font></td><td><input class="form-control" type="text" name="lastName" placeholder="LastName"/></td><tr/>
<tr><td>Password:<font color="red">*</font></td><td><input type="password" class="form-control" name="password" placeholder="Password"/></td><tr/>
<tr><td>Confirm Password:<font color="red">*</font></td><td><input type="password" class="form-control" name="cpassword" placeholder="Confirm Password"/></td><tr/>
<tr><td>Email:<font color="red">*</font></td><td><input type="text" name="email" class="form-control" placeholder="Email id"/></td><tr/>
<tr><td>BirthDate:<font color="red">*</font></td><td><input type="text" name="birthDate" class="form-control" placeholder="BirthDate"/></td><tr/>
<tr><td></td><td><font color="red"><span>Date in YYYY-MM-DD format</span></font></td></tr>
<tr><td>Gender:<font color="red">*</font></td><td><input type="radio" name="gender" value='M'>Male</input><input type="radio" name="gender" checked="checked" value='F'>Female</input></td><tr/>

</table>
<input type='submit' class="btn btn-success" value='sIGN uP'/><br/>
</form>
<%
	String id=(String)request.getParameter("id");
if(id!=null){
	if(id.equals("1"))
		out.println("<script>alert('UserName unavailable. Please Try with a different userName');</script>");
	if(id.equals("2"))
		out.println("<script>alert('Invalid UserName or Password');</script>");
	if(id.equals("3"))
		out.println("<script>alert('Passwords don't match');</script>");
	if(id.equals("4"))
		out.println("<script>alert('Date should be in yyyy-MM-dd format');</script>");
	if(id.equals("5"))
		out.println("<script>alert('All fields are mandatory');</script>");
	if(id.equals("0"))
		out.println("<script>alert('Registration successful');</script>");
}
	
%>
</body>
</html>