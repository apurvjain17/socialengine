<%@page import="presentationTier.CommentTO"%>
<%@page import="java.util.List"%>
<%@page import="presentationTier.PostTO"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SocialEngine1.0</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<style>
body{
background-color:white;
font-family: 'Open Sans', sans-serif;
}
#container{
position:fixed;
top:15%;
width:99%;
height:75%;
overflow: auto;
padding-bottom: 3%;
}

#logout{
margin:0%;
border:0%;
position:fixed;
top:0%;
left:0%;
height:10%;
width:100%;
color:white;
z-index:1000;
background-color:black;
}
#logoutlink{
	top:3%;
	right:0%;
	height:5%;
	position:fixed;
	color:blue;
}
#comment{

color:green;

}
#post{
position:relative;
top:5%;
padding:1%;
left:2%;
color:blue;
border-style:dashed;
border-color:black;
border-width: 2px;
width:80%;
}
</style>
</head>
<body>
<%!
List<CommentTO> list=null;
PostTO pto=null;
%>
<%
	list=(List<CommentTO>) request.getAttribute("list");
	pto=(PostTO)request.getAttribute("post");
	String userName=(String)session.getAttribute("userName");
	if(userName==null)
		response.sendRedirect("login.jsp");
%>
<div id="logout">
<span id="logoutlink">
<a id="logoutlink" class="btn btn-default" href="retrieve">Back to Home </a>
</span>
</div>
<div id="container">

<div id="post">
<h5><font size="2pt" color="black"><%=pto.getUserName()%> @ <%= pto.getTimestamp()%></font></h5>
	<h3><%= pto.getPost()%></h3>
	<font color="black"><h4><%=pto.getLikecnt()%> likes <%=pto.getCommentcnt()%> comments</h4></font>
	<c:forEach items="${list}" var="item"> 
	<div id="comment">
	${item.userName} says:- 
	${item.comment}
	</div>
	<br>
	</c:forEach>
	<form action="comment"  method="post">
	<input type="hidden" class="form-control" value="<%=pto.getPostId() %>" name="postid"/>
	<textarea rows="4" cols="100" name="comment" placeholder="Your comment goes here"></textarea>
	</br><input type="submit" class="btn btn-primary" value="comment"/>
	</form>
</div> 
</div>
</body>
</html>