<%@page import="presentationTier.UserTO"%>
<%@page import="presentationTier.PostTO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!DOCTYPE html>
<html>
<%!

	List<PostTO> list=null;
	UserTO usr=null;
	String userName=null;
%>
<% 
	userName=(String)session.getAttribute("userName");
	list=(List<PostTO>)request.getAttribute("list");
	usr=(UserTO)request.getAttribute("user");
	if(userName==null){
		response.sendRedirect("login.jsp");
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SocialEngine1.0</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<style>
body{
background-color:white;
font-family: 'Open Sans', sans-serif;
}
#container{
position:fixed;
top:15%;
width:99%;
height:75%;
overflow: auto;
padding-bottom: 3%;
}

#logout{
margin:0%;
border:0%;
position:fixed;
top:0%;
left:0%;
height:10%;
width:100%;
color:white;
z-index:1000;
background-color:black;
}
#logoutlink{
	top:3%;
	right:0%;
	position:fixed;
	color:blue;
}
#post{
position:relative;
top:5%;
padding:1%;
left:2%;
color:blue;
border-style:dashed;
border-color:black;
border-width: 2px;
width:80%;
}
</style>
</head>
<body>
<div id="logout">
<span id="logoutlink">
<a id="logoutlink" class="btn btn-default" href="retrieve">Back to Home </a>
</span>
</div>
<div id="container">
<%
if(list==null){
%>
<h1>The searched user doesn't exist</h1>
<%
	}
else{
%>

<c:forEach items="${list}" var="item">
<div id="post">
<h5><font size="2pt" color="black"> ${item.userName} @ ${item.timestamp}</font></h5>
	<h3>${item.post}</h3>
	<font color="black"><h4><span id='likecnt${item.postId}'>${item.likecnt} </span>Likes ${item.commentcnt} Comments
	<form action="comment" method="post"> <input type="hidden" value="${item.postId}" name="postid"/> <input type="submit" class="btn btn-warning" value="Comment"></form>
	<button value="Like" class="btn btn-success" id='like${item.postId}'> Like Me!</button>&nbsp;&nbsp;&nbsp;&nbsp;<button class="btn btn-danger" id='dislike${item.postId}'> DisLike Me:(</button>
	<script type="text/javascript">
	$('#like${item.postId}').click(function(){
		$.post("like",{userName:"${userName}", postid:"${item.postId}" },function(data){$('#likecnt${item.postId}').html(data);});
	});
	$('#dislike${item.postId}').click(function(){
		$.post("disLike",{userName:"${userName}", postid:"${item.postId}" },function(data){$('#likecnt${item.postId}').html(data);});
	});
	</script> 
	</h4></font>
</div>    
</br>
</c:forEach>
<%
}
%>
</div>
</body>
</html>