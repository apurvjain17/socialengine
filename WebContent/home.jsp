<%@page import="presentationTier.UserTO"%>
<%@page import="java.util.List"%>
<%@page import="presentationTier.PostTO" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SocialEngine1.0</title>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<style>
body{
background-color:white;
font-family: 'Open Sans', sans-serif;
}
#logout{
margin:0%;
border:0%;
position:fixed;
top:0%;
left:0%;
height:10%;
width:100%;
color:white;
z-index:1000;
background-color:black;
}
#logoutlink{
	top:3%;
	right:2%;
	position:fixed;
	color:blue;
}
#post{

position:relative;
top:5%;
padding:1%;
left:2%;
color:blue;
border-style:dashed;
border-color:black;
border-width: 2px;
width:80%;
}
#container{
position:fixed;
top:40%;
width:99%;
height:55%;
overflow: auto;
padding-bottom: 3%;
}
#form2con{
width:100%;
z-index:999;
background-color: white;
position:fixed;
top:11%;
left:2%;
}
#form1{
top:1%;
left:2%;
position:fixed;
}
</style>
</head>
<body>
<%!
UserTO uto=null;
String userName=null;
List<PostTO> list=null;
%>
<%
	uto=(UserTO)session.getAttribute("user");
	String userName=(String)session.getAttribute("userName");
	list=(List<PostTO>)request.getAttribute("list");
	if(userName==null)
		response.sendRedirect("login.jsp");
%>
<div id="logout"><form id="form1" action="search" method="post" class="form-inline">
<input type="text" class="form-control" name="search" placeholder="Search">
<input type="submit" class="btn btn-primary" value="search"/>
</form><a id="logoutlink" class="btn btn-default" href="logout.jsp">logout </a></div>
<div id="form2con">
<form id="form2" class="form-inline" action="post" method="post" class="form-inline">
<textarea rows="5" cols="60" class="form-control" name="post" placeholder="Tell me something new!!"></textarea><br>
<br><input type="submit" class="btn btn-primary btn-lg"  value="Post">
</form>
</div>
<div id="container">
<c:forEach items="${list}" var="item">
<div id="post">
<h5><font size="2pt" color="black"> ${item.userName} @ ${item.timestamp}</font></h5>
	<h3>${item.post}</h3>
	<font color="black"><h4><span id='likecnt${item.postId}'>${item.likecnt} </span> likes ${item.commentcnt} Comments
	&nbsp;&nbsp;&nbsp;&nbsp;<form action="comment" method="post"> <input type="hidden" value="${item.postId}" name="postid"/><input type="submit" class="btn btn-warning" value="Comment"></form>
	<button value="Like" id='like${item.postId}' class="btn btn-success"> Like Me!</button>&nbsp;&nbsp;&nbsp;&nbsp;<button id='dislike${item.postId}' class="btn btn-danger"> DisLike Me:(</button>
	 </h4></font>
	<script>
	$('#like${item.postId}').click(function(){
		$.post("like",{userName:"${item.userName}", postid:"${item.postId}" },function(data){$('#likecnt${item.postId}').html(data);});
	});
	$('#dislike${item.postId}').click(function(){
		$.post("disLike",{userName:"${userName}", postid:"${item.postId}" },function(data){$('#likecnt${item.postId}').html(data);});
	});
	</script>
</div>    
</br>
</c:forEach>
</div>
</body>
</html>